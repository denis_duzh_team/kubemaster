const storage: any = {
  nodes: [],
  containers: [],
  services: []
};

export const set = (key: string, value: any) => {
  (storage as any)[key] = value;
};

export const get = (key: string): any => {
  return (storage as any)[key];
};

export const push = (key: string, value: any) => {
  const _storage = storage as any;
  if (!_storage[key]) _storage[key] = [];
  _storage[key].push(value);
};
