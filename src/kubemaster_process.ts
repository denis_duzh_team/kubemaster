import express from "express";
import bodyParser from "body-parser";
import { Server } from "http";

import Kubemaster from "./Kubemaster";

let kubemaster: Kubemaster;
let server: Server;

export const bootstrap = async () => {
  kubemaster = new Kubemaster();

  const app = express();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.get("/register", async (req, res) => {
    const [, , , nodeIP] = req.ip.split(":");
    const node = await kubemaster.registerNode(nodeIP);
    const { address, maskBits } = node.subnet;

    res.send(`${address}/${maskBits}`);
  });

  app.post("/deployment", async (req, res) => {
    const { spec } = req.body;
    await kubemaster.deployment(spec);

    res.send("Ok");
  });

  server = app.listen(3000);
};

export const cleanup = () => {
  kubemaster?.destroy();
  server?.close();
};
