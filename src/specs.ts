export interface ContainerSpec {
  label: string;
  image: string;
}

export interface ServiceSpec {
  label: string;
  domain: string;
  port: number;
  targetPort: number;
}

export interface DeploymentSpec {
  containers: ContainerSpec[];
  services: ServiceSpec[];
}

export type Spec = ContainerSpec | ServiceSpec | DeploymentSpec;
