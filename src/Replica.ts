import { get } from "./storage";
import Container from "./Container";

export default class Replica {
  private destroyed = false;

  constructor() {
    this.check();
  }

  private async check() {
    let containers: Container[] = get("containers");
    containers = await Promise.all(
      containers.map(async container => {
        const { node } = container;
        if (!(await node.checkContainer(container))) await node.runContainer(container);

        return container;
      })
    );

    setTimeout(() => this.destroyed && this.check(), 3000);
  }

  public destroy() {
    this.destroyed = true;
  }
}
