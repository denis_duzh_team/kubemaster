import IPv4 from "./IPv4";
import { ContainerSpec } from "./specs";
import Node from "./Node";

export default class Container {
  private _ip: IPv4;
  private _spec: ContainerSpec;
  private _node: Node;

  constructor(spec: ContainerSpec, node: Node) {
    this._spec = spec;
    this._node = node;
  }

  public get ip(): IPv4 {
    return this._ip;
  }

  public get spec(): ContainerSpec {
    return this._spec;
  }

  public set ip(_ip: IPv4) {
    this._ip = _ip;
  }

  public get node(): Node {
    return this._node;
  }

  public set node(_node: Node) {
    this._node = _node;
  }
}
