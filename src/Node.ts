import { promisify } from "util";
import { exec as _exec } from "child_process";

const exec = promisify(_exec);

import IPv4 from "./IPv4";
import Kubelet from "./Kubelet";
import Container from "./Container";
import Service from "./Service";

export default class Node {
  private _nodeIP: IPv4;
  private _subnet: IPv4;
  private _kubelet = new Kubelet(this, 3000);

  constructor(nodeIP: IPv4, subnet: IPv4) {
    this._nodeIP = nodeIP;
    this._subnet = subnet;
  }

  public async setRouting() {
    await exec(
      `ip route add ${this._subnet.address}/${this._subnet.maskBits} via ${this._nodeIP.address}`
    );
  }

  public async runContainer(container: Container) {
    await this._kubelet.runContainer(container);
  }

  public async checkContainer(container: Container): Promise<boolean> {
    return await this._kubelet.checkContainer(container);
  }

  public async stopContainer(container: Container) {
    await this._kubelet.stopContainer(container);
  }

  public async addService(service: Service) {
    this._kubelet.addService(service);
  }

  public async removeService(service: Service) {
    this._kubelet.removeService(service);
  }

  public async destroy() {
    await exec(
      `ip route del ${this._subnet.address}/${this._subnet.maskBits} via ${this._nodeIP.address}`
    );
  }

  public get ip(): IPv4 {
    return this._nodeIP;
  }

  public get subnet(): IPv4 {
    return this._subnet;
  }
}
