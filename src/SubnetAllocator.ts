import IPv4 from "./IPv4";

export default class IPAllocator {
  private lastService = new IPv4("10.1.0.0", 16);
  private lastSubnet = new IPv4("10.1.0.0", 16);

  public nextSubnet(): IPv4 {
    this.lastSubnet = new IPv4(this.lastSubnet.address, this.lastSubnet.maskBits);
    const [byte3, byte2, byte1, byte0] = this.lastSubnet.addressBytes;
    this.lastSubnet.addressBytes = [byte3, byte2 + 1, byte1, byte0];

    return this.lastSubnet;
  }

  public nextService(): IPv4 {
    this.lastService = new IPv4(this.lastService.address, this.lastService.maskBits);
    const [byte3, byte2, byte1, byte0] = this.lastService.addressBytes;
    this.lastService.addressBytes = [byte3, byte2, byte1, byte0 + 1];

    return this.lastService;
  }
}
