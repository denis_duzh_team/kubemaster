import axios from "axios";

import Container from "./Container";
import IPv4 from "./IPv4";
import Service from "./Service";
import Node from "./Node";

export default class Kubelet {
  private _node: Node;
  private _port: number;

  constructor(node: Node, port: number) {
    this._node = node;
    this._port = port;
  }

  public async runContainer(container: Container) {
    const { address } = this._node.ip;
    const { data: ipString } = await axios.post(`http://${address}:${this._port}/containers`, {
      image: container.spec.image
    });

    container.ip = new IPv4(ipString);
    container.node = this._node;
  }

  public async checkContainer(container: Container) {
    const { address } = this._node.ip;
    const { data: running } = await axios.get(
      `http://${address}:${this._port}/containers/${container.ip.address}/running`
    );
    return Boolean(running);
  }

  public async stopContainer(container: Container) {
    const { address } = this._node.ip;
    await axios.delete(`http://${address}:${this._port}/containers/${container.ip.address}`);
  }

  public async addService(service: Service) {
    const { address } = this._node.ip;
    await axios.post(`http://${address}:${this._port}/services`, {
      domain: service.spec.domain,
      ip: service.ip.address
    });
  }

  public async removeService(service: Service) {
    const { address } = this._node.ip;
    try {
      await axios.delete(`http://${address}:${this._port}/services/${service.ip.address}`);
    } catch {}
  }
}
