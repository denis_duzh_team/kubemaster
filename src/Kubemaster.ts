import { isEqual } from "lodash";

import IPAllocator from "./SubnetAllocator";
import IPv4 from "./IPv4";
import Node from "./Node";
import Scheduler from "./Scheduler";
import Container from "./Container";
import Service from "./Service";
import { get, push, set } from "./storage";
import { ContainerSpec, ServiceSpec, DeploymentSpec, Spec } from "./specs";
import Replica from "./Replica";

export default class Kubemaster {
  private ipAllocator: IPAllocator = new IPAllocator();
  private scheduler: Scheduler = new Scheduler();
  private replica: Replica = new Replica();

  public async registerNode(nodeIP: string): Promise<Node> {
    const node = new Node(new IPv4(nodeIP), this.ipAllocator.nextSubnet());
    await node.setRouting();
    push("nodes", node);

    return node;
  }

  private async runContainer(spec: ContainerSpec): Promise<Container> {
    const node = this.scheduler.node();
    const container = new Container(spec, node);

    if (node) await node.runContainer(container);
    return container;
  }

  public async addService(spec: ServiceSpec) {
    const service = new Service(this.ipAllocator.nextService(), spec);
    await service.init();

    return service;
  }

  public async deployment(spec: DeploymentSpec) {
    await this.deployResources<Container>(
      spec.containers,
      "containers",
      (containerSpec: ContainerSpec) => this.runContainer(containerSpec),
      (container: Container) => container.node?.stopContainer(container)
    );

    await this.deployResources<Service>(
      spec.services,
      "services",
      (serviceSpec: ServiceSpec) => this.addService(serviceSpec),
      (service: Service) => service.destroy()
    );
  }

  public async deployResources<T extends { spec: Spec }>(
    specs: Spec[],
    resourceName: string,
    create: (spec: Spec) => Promise<T>,
    remove: (resource: T) => Promise<void>
  ) {
    const actual: T[] = get(resourceName);
    const desired: T[] = [];

    await Promise.all(
      specs.map(async spec => {
        let runningResource = actual.find(resource => isEqual(resource.spec, spec));

        if (runningResource) {
          actual.splice(actual.indexOf(runningResource), 1);
        } else {
          runningResource = await create(spec);
        }

        desired.push(runningResource);
      })
    );

    await Promise.all(
      actual.map(async resource => !desired.includes(resource) && (await remove(resource)))
    );

    set(resourceName, desired);
  }

  public async destroy() {
    const nodes: Node[] = get("nodes");
    await Promise.all(nodes.map(node => node.destroy()));

    const services: Service[] = get("services");
    await Promise.all(services.map(service => service.destroy()));

    this.replica.destroy();
  }
}
