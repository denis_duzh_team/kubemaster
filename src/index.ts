import { bootstrap, cleanup } from "./kubemaster_process";

bootstrap().catch(error => {
  console.log(error);
  process.exit(1);
});

process.on("SIGINT", cleanup);
