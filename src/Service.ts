import { promisify } from "util";
import { exec as _exec } from "child_process";
import net, { Socket } from "net";

const exec = promisify(_exec);

import IPv4 from "./IPv4";
import Node from "./Node";
import { ServiceSpec } from "./specs";
import Container from "./Container";
import { get } from "./storage";

export default class Service {
  private _ip: IPv4;
  private _spec: ServiceSpec;
  private _server = net.createServer();
  private _currentContainer: Container;

  constructor(ip: IPv4, spec: ServiceSpec) {
    this._ip = ip;
    this._spec = spec;
  }

  public async init() {
    await exec(`ip addr add ${this._ip.address}/${this._ip.maskBits} dev eth0`);

    this._server.on("connection", socket => {
      if (socket.localAddress.split(":")[3] === this._ip.address) {
        const container = this.nextContainer();
        if (container) {
          const containerSocket = new Socket();
          containerSocket.connect(this._spec.targetPort, container.ip.address, () =>
            socket.pipe(containerSocket).pipe(socket)
          );
        } else socket.end("HTTP/1.1 500 ERR\r\n\r\nNo containers available under label");
      } else socket.end("HTTP/1.1 500 ERR\r\n\r\nInvalid service requested");
    });

    await new Promise(resolve => this._server.listen(this._spec.port, resolve));

    const nodes: Node[] = get("nodes");
    await Promise.all(nodes.map(node => node.addService(this)));
  }

  private nextContainer(): Container {
    const containers: Container[] = get("containers");
    const labeledContainers = containers.filter(
      container => container.spec.label === this._spec.label
    );
    if (!labeledContainers) return null;

    this._currentContainer =
      labeledContainers[
        (labeledContainers.indexOf(this._currentContainer) + 1) % labeledContainers.length
      ];

    return this._currentContainer;
  }

  public async destroy() {
    await exec(`ip addr del ${this._ip.address}/${this._ip.maskBits} dev eth0`);
    await new Promise(resolve => this._server.close(resolve));

    const nodes: Node[] = get("nodes");
    await Promise.all(nodes.map(node => node.removeService(this)));
  }

  public get spec(): ServiceSpec {
    return this._spec;
  }

  public get ip(): IPv4 {
    return this._ip;
  }
}
