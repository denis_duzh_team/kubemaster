import { get } from "./storage";
import Node from "./Node";

export default class Scheduler {
  private currentNode: Node;

  public node(): Node {
    const nodes: Node[] = get("nodes");
    if (nodes.length > 0) {
      const index = nodes.indexOf(this.currentNode);
      this.currentNode = nodes[(index + 1) % nodes.length];
      return this.currentNode;
    }
  }
}
